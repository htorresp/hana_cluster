# HANA_Cluster



## Objective
 
This role is intented to build a HANA cluster using pacemaker with VMs running RHEL8 over Azure Cloud
The current community roles are written to run on RHEL7 however many consideration must be done
 in order to run over RHEL8

## Requirements
 
As described this role has been modified to build a HANA cluster however following requirement
are needed
- [ ] HANA System Replication
- [ ] Azure STONITH agent configured
More information about Azure STONITH can be found at https://docs.microsoft.com/en-us/azure/virtual-machines/workloads/sap/high-availability-guide-rhel-pacemaker

##Required variables

All the required variables are:

- sap_hana_ha_pacemaker_az_login_id: YOUR ID
- sap_hana_ha_pacemaker_az_password: YOUR PASSWORD
- sap_hana_ha_pacemaker_az_password_subscription_id: YOUR SUBCRIPTION
- sap_hana_ha_pacemaker_az_resource_group: YOUR RESOURCE GROUP
- sap_hana_ha_pacemaker_az_tenant_id: YOUR TENANT
- sap_hana_ha_pacemaker_cluster_name: hana_cluster
- sap_hana_ha_pacemaker_fence_agents_azure: true
- sap_hana_ha_pacemaker_hacluster_password: hacluster
- sap_hana_ha_pacemaker_hana_instance_number: '02'
- sap_hana_ha_pacemaker_hana_sid: ERP
- sap_hana_ha_pacemaker_node1_az_vmname: node
- sap_hana_ha_pacemaker_node1_fqdn: node1.com
- sap_hana_ha_pacemaker_node1_ip: xxx.xxx.xxx.xxx
- sap_hana_ha_pacemaker_node2_az_vmname: node2
- sap_hana_ha_pacemaker_node2_fqdn: node2.com
- sap_hana_ha_pacemaker_node2_ip: xx.xx.xx.xx
- sap_hana_ha_pacemaker_threshold: true
- sap_hana_ha_pacemaker_use_e4s: false
- sap_hana_ha_pacemaker_vip: xx.xx.xx.xx


## Metrics

In order to do show the power of automation, the creation of one HANA Cluster takes around 8 hour, using ansible to create it automatically takes less than 5 minutes.


## Automating more SAP
There are another community SAP roles that can be used. however a review a some modification will be required in other to run on RHEL8 however automating them can improve the SAP migration process customer are executing in nowdays

